module.exports = {
  displayName: 'app-admin',
  preset: '../../jest.preset.js',
  transform: {
    '^.+.vue$': '@vue/vue3-jest',
    '.+.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$':
      'jest-transform-stub',
    '^.+.tsx?$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'vue', 'js', 'json'],
  coverageDirectory: '../../coverage/./app/admin',
  snapshotSerializers: ['jest-serializer-vue'],
  globals: {
    'ts-jest': {
      tsconfig: './app/admin/tsconfig.spec.json',
      babelConfig: './app/admin/babel.config.js',
    },
    'vue-jest': {
      tsConfig: './app/admin/tsconfig.spec.json',
      babelConfig: './app/admin/babel.config.js',
    },
  },
};
